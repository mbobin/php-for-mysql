<html>
	<head>
		<title>Numbers to letters</title>
	</head>
	<body>
		<form action='index.php' method='get'>
			<input type='number' name='number' autofocus>
			<button type='submit'>Transform!</button>
		</form>
<?php
	function numere_cifre($number)
	{ 
		$num = "oups!";
			switch ($number) {
		case 0:
				$num = "zero";
				break;
    case 1:
        $num = "unu";
        break;
    case 2:
        $num = "doi";
        break;
    case 3:
        $num = "trei";
        break;
    case 4:
        $num = "patru";
        break;
    case 5:
        $num = "cinci";
      	break;
    case 6:
        $num = "sase";
        break;
    case 7:
        $num = "sapte";
        break;
    case 8:
        $num = "opt";
        break;
    case 9:
        $num = "noua";
        break;
		}
		return $num;
	}

	function numere_zeci($number)
	{ 
		$num = "oups!";
		if($number < 10)
		{
			$num = numere_cifre($number);
		}
		elseif($number == 10)
		{
			$num = "zece";
		}
		elseif($number == 11)
		{
				$num = "unsprezece";
		}
		elseif(($number >= 12 && $number <= 13) || $number == 15 || ($number >= 17 && $number <= 19) )
		{
			$num =  numere_cifre($number%10)."sprezece";
		}
		elseif($number == 14)
		{
			$num = "paispezece";
		}
		elseif($number == 16)
		{
			$num = "saisprezece";
		}
		elseif($number%10 == 0)
		{
			$num = numere_cifre(($number/10)%10)."zeci";
		}
		elseif($number%10 != 0)
		{
			$num = numere_cifre(($number/10)%10)."zeci si ".numere_cifre($number%10);
		}
		return $num;	
	}

	function numere_sute($number)
	{
		$num = "oups!";
		if($number < 100)
		{
			$num = numere_zeci($number);
		}
		elseif($number == 100){
			$num = "o suta";
		}
		elseif($number > 100 && $number < 200)
		{	
			if((($number%100)/10)%10 == 0)
			{
				$num = "o suta ".numere_cifre($number%100);
			}
			elseif($number == 120)
			{
				$num = "o suta douazeci";
			}
			else
			{
				$num = "o suta ".numere_zeci($number%100);
			}
		}
		elseif($number == 200){
			$num = "doua sute";
		}
		elseif($number > 200 && $number < 300)
		{
			if((($number%100)/10)%10 == 0)
				$num = "doua sute ".numere_cifre($number%100);
			else
				$num = "doua sute ".numere_zeci($number%100);
		}
		elseif($number % 100 == 0){
			$num =  numere_cifre(($number/100)%10)." sute";
		}
		elseif($number > 300 && $number <= 999)
		{
			if(($number%100)/10 == 0)
				$num = numere_cifre(($number/100)%10)." sute ".numere_cifre($number%100);
			else
				$num = numere_cifre(($number/100)%10)." sute ".numere_zeci($number%100);
		}
		return $num;
	}

	function numere_mii($number)
	{
		$num = "oups!";
		if($number < 1000)
		{
			$num = numere_sute($number);
		}
		elseif($number == 1000)
		{
			$num = "o mie";
		}
		elseif($number > 1000 && $number < 2000)
		{
			$num ="o mie ".numere_sute($number%1000);
		}
		elseif($number == 2000)
		{
			$num = "doua mii";
		}
		elseif($number > 2000 && $number < 3000)
		{
			$num ="doua mii ".numere_sute($number%1000);
		}	
		elseif($number % 1000 == o)
		{
			$num = numere_cifre(($number/1000)%10)." mii";
		}
		elseif($number > 3000 && $number < 10000)
		{
			$num =numere_cifre(($number/1000)%10)." mii ".numere_sute($number%1000);
		}
		return $num;
	}

	function numere_sute_mii($number)
	{
		$nr="oups!";
		if($number >= 10000 && $number < 1000000 )
		{
			$nr= numere_sute(($number/1000)%1000)." mii";

			if ($number%1000 != 0)
				$nr = $nr." ".numere_sute($number%1000);
		}
		elseif ($number < 10000)
		{
			$nr = numere_mii($number);
		}
		return $nr;
	}

	function numere_milioane($number)
	{
		$nr="oups!";
		if($number < 1000000)
		{
			$nr = numere_sute_mii($number); 
		}
		elseif($number == 1000000)
		{
			$nr = "un milion";
		}
		elseif($number > 1000000 && $number < 2000000)
		{
			$nr = "un milion ".numere_sute_mii($number%1000000);
		}
		elseif($number == 2000000)
		{
			$nr = "doua milioane";
		}
		elseif($number > 2000000 && $number < 3000000)
		{
			$nr = "doua milioane ".numere_sute_mii($number%1000000);
		}
		elseif($number%1000000 == 0)
		{
			$nr = numere_cifre($number/1000000)." milioane";
		}
		elseif($number > 3000000 && $number < 100000000)
		{
			$nr = numere_cifre(($number/1000000)%10)." milioane ".numere_sute_mii($number%1000000);
		}


		return $nr;
	}

	$number = $_GET['number'];
	echo numere_milioane($number);

?>
	</body>
</html>