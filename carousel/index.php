<html>
	<head>
		<title>Php Carousel</title>
		<style type="text/css">
			body
			{
				background-color: #EBEEF2;
				width: 100%;
				margin: 0 0;
			}

			div.menu
			{
				text-align: center;
				padding-top: 15px;
				padding-bottom: 25px;
			}

			div.menu a
			{
				width: 25px;
				margin-right: 5px;
			}
			div.container
			{
				height: 525px;
			}

			img{
				max-width: 750px;
				display:block;
				margin: 0 auto;
				margin-bottom: 25px;
			}

			a
			{
 				appearance: button;
    		-moz-appearance: button;
    		-webkit-appearance: button;
    		text-decoration: none;
    		font: menu;
    		color: ButtonText;
    		display: inline-block; padding: 2px 8px;
			}  

			a.Previous,
			a.next
			{
				position: fixed;
				top: 250px;
			}

			a.Previous
			{
				left: 100px;
			}

			a.next
			{
				right: 100px;
			}

			form
			{
				display: block;
				margin: 50px auto;
				width: 100%;
			}
			
			input,
			button
			{
				width: 120px;
				margin: 5px auto;
				display: block;
			}
		</style>
	</head>
	<body>	
		<?php

			$dir='pics/';
			$images = glob($dir . "*.jpg");

			$id = $_GET["id"];     // page id
			$many = $_GET["many"]; // how many images per page

			echo "<div class='container'>";


			if(empty($many) == 1)
			{
				$many = 1;
			}
			if (empty($id) == 1)
			{
				$id = 0;  //must be called as /index.php
			}
			
			if(is_numeric($id) && is_numeric($many) && $id >= 0 && $id+1 <= count($images)/$many + 1 )
			{
				echo "<div class='menu'>";
				echo "<h3>How Many per page:</h3>";
				for ($i = 1; $i <= 10; $i++)
					echo "<a href='index.php?many=".$i."'>".$i."</a>";
				echo "</div>";

				for ($i = 0; $i < $many; $i++)
				{
					if($id*$many+$i < count($images))
						echo "<img src='".$images[$id*$many+$i]."' alt='picture'>";
				}
				if($id > 0)
				{
					echo  "<a href='index.php?id=".($id-1)."&many=".$many."' class='Previous'>Previous!</a>" ;
				}
?>
				<form method='get' action='index.php'>
					<input type='number' name='id' placeholder='Page number'>
					<input type='hidden' name='many' value= <?php echo $many; ?> >
					<button type='submit'>Go!</button>
				</form>
<?php

				if(($id+1)*$many < count($images))
				{
					echo  "<a class='next' href='index.php?id=".($id+1)."&many=".$many."'>Next!</a>" ;
				}
			}
			else
			{
				header("Location: index.php?id=0&many=1");
			}
?>
		</div>
	</body>
</html>


